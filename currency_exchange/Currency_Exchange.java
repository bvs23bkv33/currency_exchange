package currency_exchange;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

public class Currency_Exchange {

	static int start_UAH = 0;
	static int start_USD = 0;
	static int start_EUR = 0;
	static int start_RUR = 0;
    static int year;
    static int month;
    static int day;
    
    static int line;
    
    static int USDplus;
    static int USDminus;
    static int EURplus;
    static int EURminus;
    static int RURplus;
    static int RURminus;
    static int UAHline;
	
	class operation implements Serializable {
		private static final long serialVersionUID = -1396029386419822921L;
		public int type;
		public Date date;
		public int currency;
		public int sum;
		public double rate;
		public String comment;
		
		public operation() {
		};
		
		public operation(int ty, Date da, int cu, int su, double ra, String co) {
			type = ty;
			date = da;
			currency = cu;
			sum = su;
			rate = ra;
			comment = co;
		};
		
		public operation(int ty, Date da, int cu, int su, double ra) {
			type = ty;
			date = da;
			currency = cu;
			sum = su;
			rate = ra;
		};
		
	}
	
	static ArrayList<operation> operations;
	
	static void fill(JPanel p, Color c) {
        p.setLayout(new GridBagLayout());
        JLabel label;
    	Font font;
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.anchor = GridBagConstraints.PAGE_START;
		
    	label = new JLabel("������� �� ������", JLabel.CENTER);
    	label.setBackground(c);
    	label.setOpaque(true);
    	gbc.gridwidth = 3;
    	gbc.gridx = 0;
    	gbc.gridy = 0;
    	p.add(label, gbc);
    	
    	label = new JLabel("0,00", JLabel.CENTER);
    	label.setBackground(c);
    	label.setOpaque(true);
    	gbc.gridwidth = 1;
    	gbc.gridx = 3;
    	gbc.gridy = 0;
    	p.add(label, gbc);
    	
    	label = new JLabel("������� �� �����", JLabel.CENTER);
    	label.setBackground(c);
    	label.setOpaque(true);
    	gbc.gridwidth = 3;
    	gbc.gridx = 4;
    	gbc.gridy = 0;
    	p.add(label, gbc);
    	
    	label = new JLabel("0,00", JLabel.CENTER);
    	label.setBackground(c);
    	label.setOpaque(true);
    	gbc.gridwidth = 1;
    	gbc.gridx = 7;
    	gbc.gridy = 0;
    	p.add(label, gbc);
    	
    	label = new JLabel("����� �������", JLabel.CENTER);
    	font = label.getFont();
    	label.setFont(font.deriveFont(Font.PLAIN));
    	label.setBackground(c);
    	label.setOpaque(true);
    	gbc.gridwidth = 3;
    	gbc.gridx = 0;
    	gbc.gridy = 1;
    	p.add(label, gbc);
    	
    	label = new JLabel("0,00", JLabel.CENTER);
    	label.setBackground(c);
    	label.setOpaque(true);
    	gbc.gridwidth = 1;
    	gbc.gridx = 3;
    	gbc.gridy = 1;
    	p.add(label, gbc);
    	
    	label = new JLabel("���� �������", JLabel.CENTER);
    	font = label.getFont();
    	label.setFont(font.deriveFont(Font.PLAIN));
    	label.setBackground(c);
    	label.setOpaque(true);
    	gbc.gridwidth = 3;
    	gbc.gridx = 0;
    	gbc.gridy = 2;
    	p.add(label, gbc);
    	
    	label = new JLabel("0,00", JLabel.CENTER);
    	label.setBackground(c);
    	label.setOpaque(true);
    	gbc.gridwidth = 1;
    	gbc.gridx = 3;
    	gbc.gridy = 2;
    	p.add(label, gbc);
    	
    	label = new JLabel("����� ������", JLabel.CENTER);
    	font = label.getFont();
    	label.setFont(font.deriveFont(Font.PLAIN));
    	label.setBackground(c);
    	label.setOpaque(true);
    	gbc.gridwidth = 3;
    	gbc.gridx = 4;
    	gbc.gridy = 1;
    	p.add(label, gbc);
    	
    	label = new JLabel("0,00", JLabel.CENTER);
    	label.setBackground(c);
    	label.setOpaque(true);
    	gbc.gridwidth = 1;
    	gbc.gridx = 7;
    	gbc.gridy = 1;
    	p.add(label, gbc);
    	
    	label = new JLabel("���� �������", JLabel.CENTER);
    	font = label.getFont();
    	label.setFont(font.deriveFont(Font.PLAIN));
    	label.setBackground(c);
    	label.setOpaque(true);
    	gbc.gridwidth = 3;
    	gbc.gridx = 4;
    	gbc.gridy = 2;
    	p.add(label, gbc);
    	
    	label = new JLabel("0,00", JLabel.CENTER);
    	label.setBackground(c);
    	label.setOpaque(true);
    	gbc.gridwidth = 1;
    	gbc.gridx = 7;
    	gbc.gridy = 2;
    	p.add(label, gbc);
    	
    	label = new JLabel("�������� �������", JLabel.CENTER);
    	label.setBackground(Color.YELLOW);
    	label.setOpaque(true);
    	gbc.gridwidth = 4;
    	gbc.gridx = 0;
    	gbc.gridy = 4;
    	p.add(label, gbc);
    	
    	label = new JLabel("�������� �������", JLabel.CENTER);
    	label.setBackground(Color.YELLOW);
    	label.setOpaque(true);
    	gbc.gridwidth = 4;
    	gbc.gridx = 4;
    	gbc.gridy = 4;
    	p.add(label, gbc);
    	
    	label = new JLabel("�����", JLabel.CENTER);
    	font = label.getFont();
    	label.setFont(font.deriveFont(Font.PLAIN));
    	label.setBackground(Color.LIGHT_GRAY);
    	label.setOpaque(true);
    	gbc.gridwidth = 1;
    	gbc.gridx = 0;
    	gbc.gridy = 5;
    	p.add(label, gbc);
    	
    	label = new JLabel("����", JLabel.CENTER);
    	font = label.getFont();
    	label.setFont(font.deriveFont(Font.PLAIN));
    	label.setBackground(Color.LIGHT_GRAY);
    	label.setOpaque(true);
    	gbc.gridwidth = 1;
    	gbc.gridx = 1;
    	gbc.gridy = 5;
    	p.add(label, gbc);
    	
    	label = new JLabel("�����", JLabel.CENTER);
    	font = label.getFont();
    	label.setFont(font.deriveFont(Font.PLAIN));
    	label.setBackground(c);
    	label.setOpaque(true);
    	gbc.gridwidth = 1;
    	gbc.gridx = 2;
    	gbc.gridy = 5;
    	p.add(label, gbc);
    	
    	label = new JLabel("������", JLabel.CENTER);
    	font = label.getFont();
    	label.setFont(font.deriveFont(Font.PLAIN));
    	label.setBackground(c);
    	label.setOpaque(true);
    	gbc.gridwidth = 1;
    	gbc.gridx = 3;
    	gbc.gridy = 5;
    	p.add(label, gbc);
    	
    	label = new JLabel("�����", JLabel.CENTER);
    	font = label.getFont();
    	label.setFont(font.deriveFont(Font.PLAIN));
    	label.setBackground(Color.LIGHT_GRAY);
    	label.setOpaque(true);
    	gbc.gridwidth = 1;
    	gbc.gridx = 4;
    	gbc.gridy = 5;
    	p.add(label, gbc);
    	
    	label = new JLabel("����", JLabel.CENTER);
    	font = label.getFont();
    	label.setFont(font.deriveFont(Font.PLAIN));
    	label.setBackground(Color.LIGHT_GRAY);
    	label.setOpaque(true);
    	gbc.gridwidth = 1;
    	gbc.gridx = 5;
    	gbc.gridy = 5;
    	p.add(label, gbc);
    	
    	label = new JLabel("�����", JLabel.CENTER);
    	font = label.getFont();
    	label.setFont(font.deriveFont(Font.PLAIN));
    	label.setBackground(c);
    	label.setOpaque(true);
    	gbc.gridwidth = 1;
    	gbc.gridx = 6;
    	gbc.gridy = 5;
    	p.add(label, gbc);
    	
    	label = new JLabel("������", JLabel.CENTER);
    	font = label.getFont();
    	label.setFont(font.deriveFont(Font.PLAIN));
    	label.setBackground(c);
    	label.setOpaque(true);
    	gbc.gridwidth = 1;
    	gbc.gridx = 7;
    	gbc.gridy = 5;
    	p.add(label, gbc);
	}
	
	static void fillDomestic(JPanel p) {
		JLabel label;
    	Font font;
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;
    	
    	label = new JLabel("������� �� ������", JLabel.CENTER);
    	gbc.gridwidth = 3;
    	gbc.gridx = 0;
    	gbc.gridy = 0;
    	p.add(label, gbc);
    	
    	label = new JLabel("0,00", JLabel.CENTER);
    	label.setForeground(Color.RED);
    	label.setOpaque(true);
    	gbc.gridwidth = 1;
    	gbc.gridx = 3;
    	gbc.gridy = 0;
    	p.add(label, gbc);
    	
    	label = new JLabel("������� �� �����", JLabel.CENTER);
    	gbc.gridwidth = 3;
    	gbc.gridx = 4;
    	gbc.gridy = 0;
    	p.add(label, gbc);
    	
    	label = new JLabel("0,00", JLabel.CENTER);
    	label.setForeground(Color.RED);
    	label.setOpaque(true);
    	gbc.gridwidth = 1;
    	gbc.gridx = 7;
    	gbc.gridy = 0;
    	p.add(label, gbc);
    	
    	label = new JLabel("����� ��������", JLabel.CENTER);
    	label.setBackground(Color.YELLOW);
    	label.setOpaque(true);
    	gbc.gridwidth = 3;
    	gbc.gridx = 0;
    	gbc.gridy = 1;
    	p.add(label, gbc);
    	
    	label = new JLabel("0,00", JLabel.CENTER);
    	label.setForeground(Color.RED);
    	label.setBackground(Color.YELLOW);
    	label.setOpaque(true);
    	gbc.gridwidth = 1;
    	gbc.gridx = 3;
    	gbc.gridy = 1;
    	p.add(label, gbc);
    	
    	label = new JLabel("����� ��������", JLabel.CENTER);
    	label.setBackground(Color.YELLOW);
    	label.setOpaque(true);
    	gbc.gridwidth = 3;
    	gbc.gridx = 4;
    	gbc.gridy = 1;
    	p.add(label, gbc);
    	
    	label = new JLabel("0,00", JLabel.CENTER);
    	label.setForeground(Color.RED);
    	label.setBackground(Color.YELLOW);
    	label.setOpaque(true);
    	gbc.gridwidth = 1;
    	gbc.gridx = 7;
    	gbc.gridy = 1;
    	p.add(label, gbc);
    	
    	label = new JLabel("�����", JLabel.CENTER);
    	font = label.getFont();
    	label.setFont(font.deriveFont(Font.PLAIN));
    	label.setBackground(Color.LIGHT_GRAY);
    	label.setOpaque(true);
    	gbc.gridwidth = 1;
    	gbc.gridx = 0;
    	gbc.gridy = 3;
    	p.add(label, gbc);
    	
    	label = new JLabel("�����", JLabel.CENTER);
    	font = label.getFont();
    	label.setFont(font.deriveFont(Font.PLAIN));
    	label.setBackground(Color.YELLOW);
    	label.setOpaque(true);
    	gbc.gridwidth = 1;
    	gbc.gridx = 1;
    	gbc.gridy = 3;
    	p.add(label, gbc);
    	
    	label = new JLabel("�����������", JLabel.CENTER);
    	font = label.getFont();
    	label.setFont(font.deriveFont(Font.PLAIN));
    	label.setBackground(Color.YELLOW);
    	label.setOpaque(true);
    	gbc.gridwidth = 6;
    	gbc.gridx = 2;
    	gbc.gridy = 3;
    	p.add(label, gbc);
	}
	
	static String getMonthNominative(int m) {
		String Result = "";
		switch (m) {
			case 0: Result = "������";
			break;
			case 1: Result = "�������";
			break;
			case 2: Result = "����";
			break;
			case 3: Result = "������";
			break;
			case 4: Result = "���";
			break;
			case 5: Result = "����";
			break;
			case 6: Result = "����";
			break;
			case 7: Result = "������";
			break;
			case 8: Result = "��������";
			break;
			case 9: Result = "�������";
			break;
			case 10: Result = "������";
			break;
			case 11: Result = "�������";
			break;
		}
		return Result;
	}
	
	static String getMonthGenitive(int m) {
		String Result = "";
		switch (m) {
			case 0: Result = "������";
			break;
			case 1: Result = "�������";
			break;
			case 2: Result = "�����";
			break;
			case 3: Result = "������";
			break;
			case 4: Result = "���";
			break;
			case 5: Result = "����";
			break;
			case 6: Result = "����";
			break;
			case 7: Result = "�������";
			break;
			case 8: Result = "��������";
			break;
			case 9: Result = "�������";
			break;
			case 10: Result = "������";
			break;
			case 11: Result = "�������";
			break;
		}
		return Result;
	}
	
	//@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		JFrame frame = new JFrame("����� �����");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setPreferredSize(new Dimension(1280, 720));    
        
        /*operations = new ArrayList<operation>();
        
        @SuppressWarnings("deprecation")
		Date date1 = new Date(115, 4, 1);
        
        operation op1 = new operation(0, date1, 980, 78500, 1.0, "��������� �������");
        
        operations.add(op1);
        
        @SuppressWarnings("deprecation")
		Date date2 = new Date(115, 4, 6);
        
        operation op2 = new operation(1, date2, 840, 500, 21.6);
        
        operations.add(op2);
        
        FileOutputStream fos = null;
		try {
			fos = new FileOutputStream("data.bin");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        try {
        	ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(operations);
			oos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
        
        FileInputStream fis = null;
		try {
			fis = new FileInputStream("data.bin");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        try {
        	ObjectInputStream ois = new ObjectInputStream(fis);
			try {
				Object temp = ois.readObject();
				if (temp instanceof ArrayList<?>) {
					operations = (ArrayList<operation>) temp;
				}
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ois.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        operation o = operations.get(0);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(o.date);
        
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DATE);
        
        JTabbedPane yearsPane = new JTabbedPane(JTabbedPane.BOTTOM);
        JTabbedPane monthsPane = new JTabbedPane(JTabbedPane.BOTTOM);
        JTabbedPane daysPane = new JTabbedPane(JTabbedPane.BOTTOM);
        frame.add(yearsPane);
        yearsPane.addTab(Integer.toString(year), monthsPane);
        monthsPane.addTab(getMonthNominative(month), daysPane);
        
    	JPanel panel = new JPanel(); 
    	panel.setBorder(BorderFactory.createTitledBorder(Integer.toString(day) + " " + getMonthGenitive(month) + " " + Integer.toString(year)));
    	daysPane.add(Integer.toString(day), panel);
    	
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
        
    	JPanel USDpanel = new JPanel(new GridBagLayout());
    	USDpanel.setBorder(BorderFactory.createTitledBorder("������"));
    	panel.add(USDpanel);
    	
    	fill(USDpanel, new Color(255, 200, 200));
    	
    	JPanel EURpanel = new JPanel(new GridBagLayout());
    	EURpanel.setBorder(BorderFactory.createTitledBorder("����"));
    	panel.add(EURpanel);
    	
    	fill(EURpanel, new Color(200, 255, 255));
    	
    	JPanel RURpanel = new JPanel(new GridBagLayout());
    	RURpanel.setBorder(BorderFactory.createTitledBorder("�����"));
    	panel.add(RURpanel);
    	
    	fill(RURpanel, new Color(200, 255, 200));
    	
    	JPanel UAHpanel = new JPanel(new GridBagLayout());
    	UAHpanel.setBorder(BorderFactory.createTitledBorder("������"));
    	panel.add(UAHpanel);
        UAHpanel.setLayout(new GridBagLayout());
        
        fillDomestic(UAHpanel);
        
        JLabel label;
        Font font;
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;
    	
        for (int i = 1; i < operations.size(); i++) {
        	o = operations.get(i);
        	
    		calendar.setTime(o.date);
            
            if (year == calendar.get(Calendar.YEAR)) {
            	if (month == calendar.get(Calendar.MONTH)) {
            		if (day == calendar.get(Calendar.DATE)) {
            			switch (o.currency) {
            			case 840 : 
            				if (o.sum >= 0) {
            					label = new JLabel(Integer.toString(Calendar.HOUR) + ":" + Integer.toString(Calendar.MINUTE), JLabel.CENTER);
            			    	font = label.getFont();
            			    	label.setFont(font.deriveFont(Font.PLAIN));
            			    	label.setBackground(Color.LIGHT_GRAY);
            			    	label.setOpaque(true);
            			    	gbc.gridwidth = 1;
            			    	gbc.gridx = 0;
            			    	gbc.gridy = 5;
            			    	USDpanel.add(label, gbc);
            			    	
            			    	label = new JLabel(Double.toString(o.rate), JLabel.CENTER);
            			    	font = label.getFont();
            			    	label.setFont(font.deriveFont(Font.PLAIN));
            			    	label.setBackground(Color.LIGHT_GRAY);
            			    	label.setOpaque(true);
            			    	gbc.gridwidth = 1;
            			    	gbc.gridx = 1;
            			    	gbc.gridy = 5;
            			    	USDpanel.add(label, gbc);
            			    	
            			    	label = new JLabel(Integer.toString(o.sum), JLabel.CENTER);
            			    	font = label.getFont();
            			    	label.setFont(font.deriveFont(Font.PLAIN));
            			    	label.setBackground(new Color(255, 200, 200));
            			    	label.setOpaque(true);
            			    	gbc.gridwidth = 1;
            			    	gbc.gridx = 2;
            			    	gbc.gridy = 5;
            			    	USDpanel.add(label, gbc);
            			    	
            			    	label = new JLabel(Integer.toString((int)(o.rate * o.sum)), JLabel.CENTER);
            			    	font = label.getFont();
            			    	label.setFont(font.deriveFont(Font.PLAIN));
            			    	label.setBackground(new Color(255, 200, 200));
            			    	label.setOpaque(true);
            			    	gbc.gridwidth = 1;
            			    	gbc.gridx = 3;
            			    	gbc.gridy = 5;
            			    	USDpanel.add(label, gbc);
            				} else {
            					
            				};
            			break;
            			case 978 :
            				if (o.sum >= 0) {
            					
            				} else {
            					
            				};
            			break;
            			case 643 :
            				if (o.sum >= 0) {
            					
            				} else {
            					
            				};
            			break;
            			case 980 :
            				if (o.sum >= 0) {
            					
            				} else {
            					
            				};
            			break;
            			}
            			
            			line++;
            			
            		} else {
            			
            			line = 0;
            	        year = calendar.get(Calendar.YEAR);
            	        month = calendar.get(Calendar.MONTH);
            	        day = calendar.get(Calendar.DATE);
            			
            			panel = new JPanel(); 
            	    	panel.setBorder(BorderFactory.createTitledBorder(Integer.toString(day) + " " + getMonthGenitive(month) + " " + Integer.toString(year)));
            	    	daysPane.add(Integer.toString(day), panel);
            	    	
            			panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
            	        
            	    	USDpanel = new JPanel(new GridBagLayout());
            	    	USDpanel.setBorder(BorderFactory.createTitledBorder("������"));
            	    	panel.add(USDpanel);
            	    	
            	    	fill(USDpanel, new Color(255, 200, 200));
            	    	
            	    	EURpanel = new JPanel(new GridBagLayout());
            	    	EURpanel.setBorder(BorderFactory.createTitledBorder("����"));
            	    	panel.add(EURpanel);
            	    	
            	    	fill(EURpanel, new Color(200, 255, 255));
            	    	
            	    	RURpanel = new JPanel(new GridBagLayout());
            	    	RURpanel.setBorder(BorderFactory.createTitledBorder("�����"));
            	    	panel.add(RURpanel);
            	    	
            	    	fill(RURpanel, new Color(200, 255, 200));
            	    	
            	    	UAHpanel = new JPanel(new GridBagLayout());
            	    	UAHpanel.setBorder(BorderFactory.createTitledBorder("������"));
            	    	panel.add(UAHpanel);
            	        UAHpanel.setLayout(new GridBagLayout());
            	        
            	        fillDomestic(UAHpanel);
            	        
            	        switch (o.currency) {
            			case 840 : 
            				if (o.sum >= 0) {
            					
            					label = new JLabel(Integer.toString(Calendar.HOUR) + ":" + Integer.toString(Calendar.MINUTE), JLabel.CENTER);
            			    	font = label.getFont();
            			    	label.setFont(font.deriveFont(Font.PLAIN));
            			    	label.setBackground(Color.LIGHT_GRAY);
            			    	label.setOpaque(true);
            			    	gbc.gridwidth = 1;
            			    	gbc.gridx = 0;
            			    	gbc.gridy = 6 + line;
            			    	USDpanel.add(label, gbc);
            			    	
            			    	label = new JLabel(Double.toString(o.rate), JLabel.CENTER);
            			    	font = label.getFont();
            			    	label.setFont(font.deriveFont(Font.PLAIN));
            			    	label.setBackground(Color.LIGHT_GRAY);
            			    	label.setOpaque(true);
            			    	gbc.gridwidth = 1;
            			    	gbc.gridx = 1;
            			    	gbc.gridy = 6 + line;
            			    	USDpanel.add(label, gbc);
            			    	
            			    	label = new JLabel(Integer.toString(o.sum), JLabel.CENTER);
            			    	font = label.getFont();
            			    	label.setFont(font.deriveFont(Font.PLAIN));
            			    	label.setBackground(new Color(255, 200, 200));
            			    	label.setOpaque(true);
            			    	gbc.gridwidth = 1;
            			    	gbc.gridx = 2;
            			    	gbc.gridy = 6 + line;
            			    	USDpanel.add(label, gbc);
            			    	
            			    	label = new JLabel(Integer.toString((int)(o.rate * o.sum)), JLabel.CENTER);
            			    	font = label.getFont();
            			    	label.setFont(font.deriveFont(Font.PLAIN));
            			    	label.setBackground(new Color(255, 200, 200));
            			    	label.setOpaque(true);
            			    	gbc.gridwidth = 1;
            			    	gbc.gridx = 3;
            			    	gbc.gridy = 6 + line;
            			    	USDpanel.add(label, gbc);
            				} else {
            					
            				};
            			break;
            			case 978 :
            				if (o.sum >= 0) {
            					
            				} else {
            					
            				};
            			break;
            			case 643 :
            				if (o.sum >= 0) {
            					
            				} else {
            					
            				};
            			break;
            			case 980 :
            				if (o.sum >= 0) {
            					
            				} else {
            					
            				};
            			break;
            			}
            	        
            	        line++;
            	        
            		}
            	} else {
            		
            		line = 0;
            		year = calendar.get(Calendar.YEAR);
        	        month = calendar.get(Calendar.MONTH);
        	        day = calendar.get(Calendar.DATE);
        	        
        	        daysPane = new JTabbedPane(JTabbedPane.BOTTOM);
        	        
        	        monthsPane.addTab(getMonthNominative(month), daysPane);
        	        
        	        monthsPane.setSelectedComponent(daysPane);
        			
        			panel = new JPanel(); 
        	    	panel.setBorder(BorderFactory.createTitledBorder(Integer.toString(day) + " " + getMonthGenitive(month) + " " + Integer.toString(year)));
        	    	daysPane.add(Integer.toString(day), panel);
        	    	
        	    	daysPane.setSelectedComponent(panel);
        	    	
        			panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
        	        
        	    	USDpanel = new JPanel(new GridBagLayout());
        	    	USDpanel.setBorder(BorderFactory.createTitledBorder("������"));
        	    	panel.add(USDpanel);
        	    	
        	    	fill(USDpanel, new Color(255, 200, 200));
        	    	
        	    	EURpanel = new JPanel(new GridBagLayout());
        	    	EURpanel.setBorder(BorderFactory.createTitledBorder("����"));
        	    	panel.add(EURpanel);
        	    	
        	    	fill(EURpanel, new Color(200, 255, 255));
        	    	
        	    	RURpanel = new JPanel(new GridBagLayout());
        	    	RURpanel.setBorder(BorderFactory.createTitledBorder("�����"));
        	    	panel.add(RURpanel);
        	    	
        	    	fill(RURpanel, new Color(200, 255, 200));
        	    	
        	    	UAHpanel = new JPanel(new GridBagLayout());
        	    	UAHpanel.setBorder(BorderFactory.createTitledBorder("������"));
        	    	panel.add(UAHpanel);
        	        UAHpanel.setLayout(new GridBagLayout());
        	        
        	        fillDomestic(UAHpanel);
        	        
        	        switch (o.currency) {
        			case 840 : 
        				if (o.sum >= 0) {
        					
        				} else {
        					
        				};
        			break;
        			case 978 :
        				if (o.sum >= 0) {
        					
        				} else {
        					
        				};
        			break;
        			case 643 :
        				if (o.sum >= 0) {
        					
        				} else {
        					
        				};
        			break;
        			case 980 :
        				if (o.sum >= 0) {
        					
        				} else {
        					
        				};
        			break;
        			}
        	        
        	        line++;
        	        
            	}
            } else {
            	
            	line = 0;
            	year = calendar.get(Calendar.YEAR);
    	        month = calendar.get(Calendar.MONTH);
    	        day = calendar.get(Calendar.DATE);
    	        
    	        monthsPane = new JTabbedPane(JTabbedPane.BOTTOM);
    	        
    	        yearsPane.addTab(Integer.toString(year), monthsPane);
    	        
    	        yearsPane.setSelectedComponent(monthsPane);
    	        
    	        daysPane = new JTabbedPane(JTabbedPane.BOTTOM);
    	        
    	        monthsPane.addTab(getMonthNominative(month), daysPane);
    	        
    	        monthsPane.setSelectedComponent(daysPane);
    			
    			panel = new JPanel(); 
    	    	panel.setBorder(BorderFactory.createTitledBorder(Integer.toString(day) + " " + getMonthGenitive(month) + " " + Integer.toString(year)));
    	    	daysPane.add(Integer.toString(day), panel);
    	    	
    	    	daysPane.setSelectedComponent(panel);
    	    	
    			panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
    	        
    	    	USDpanel = new JPanel(new GridBagLayout());
    	    	USDpanel.setBorder(BorderFactory.createTitledBorder("������"));
    	    	panel.add(USDpanel);
    	    	
    	    	fill(USDpanel, new Color(255, 200, 200));
    	    	
    	    	EURpanel = new JPanel(new GridBagLayout());
    	    	EURpanel.setBorder(BorderFactory.createTitledBorder("����"));
    	    	panel.add(EURpanel);
    	    	
    	    	fill(EURpanel, new Color(200, 255, 255));
    	    	
    	    	RURpanel = new JPanel(new GridBagLayout());
    	    	RURpanel.setBorder(BorderFactory.createTitledBorder("�����"));
    	    	panel.add(RURpanel);
    	    	
    	    	fill(RURpanel, new Color(200, 255, 200));
    	    	
    	    	UAHpanel = new JPanel(new GridBagLayout());
    	    	UAHpanel.setBorder(BorderFactory.createTitledBorder("������"));
    	    	panel.add(UAHpanel);
    	        UAHpanel.setLayout(new GridBagLayout());
    	        
    	        fillDomestic(UAHpanel);
    	        
    	        switch (o.currency) {
    			case 840 : 
    				if (o.sum >= 0) {
    					
    				} else {
    					
    				};
    			break;
    			case 978 :
    				if (o.sum >= 0) {
    					
    				} else {
    					
    				};
    			break;
    			case 643 :
    				if (o.sum >= 0) {
    					
    				} else {
    					
    				};
    			break;
    			case 980 :
    				if (o.sum >= 0) {
    					
    				} else {
    					
    				};
    			break;
    			}
    	        
    	        line++;
            	
            }
        	
        }
        
		calendar.setTime(new Date());
		
		if (year != calendar.get(Calendar.YEAR)) {
			
			line = 0;			
    		year = calendar.get(Calendar.YEAR);
	        month = calendar.get(Calendar.MONTH);
	        day = calendar.get(Calendar.DATE);
	        
	        monthsPane = new JTabbedPane(JTabbedPane.BOTTOM);
	        
	        yearsPane.addTab(Integer.toString(year), monthsPane);
	        
	        yearsPane.setSelectedComponent(monthsPane);
	        
	        daysPane = new JTabbedPane(JTabbedPane.BOTTOM);
	        
	        monthsPane.addTab(getMonthNominative(month), daysPane);
	        
	        monthsPane.setSelectedComponent(daysPane);
			
			panel = new JPanel(); 
	    	panel.setBorder(BorderFactory.createTitledBorder(Integer.toString(day) + " " + getMonthGenitive(month) + " " + Integer.toString(year)));
	    	daysPane.add(Integer.toString(day), panel);
	    	
	    	daysPane.setSelectedComponent(panel);
	    	
			panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
	        
	    	USDpanel = new JPanel(new GridBagLayout());
	    	USDpanel.setBorder(BorderFactory.createTitledBorder("������"));
	    	panel.add(USDpanel);
	    	
	    	fill(USDpanel, new Color(255, 200, 200));
	    	
	    	EURpanel = new JPanel(new GridBagLayout());
	    	EURpanel.setBorder(BorderFactory.createTitledBorder("����"));
	    	panel.add(EURpanel);
	    	
	    	fill(EURpanel, new Color(200, 255, 255));
	    	
	    	RURpanel = new JPanel(new GridBagLayout());
	    	RURpanel.setBorder(BorderFactory.createTitledBorder("�����"));
	    	panel.add(RURpanel);
	    	
	    	fill(RURpanel, new Color(200, 255, 200));
	    	
	    	UAHpanel = new JPanel(new GridBagLayout());
	    	UAHpanel.setBorder(BorderFactory.createTitledBorder("������"));
	    	panel.add(UAHpanel);
	        UAHpanel.setLayout(new GridBagLayout());
	        
	        fillDomestic(UAHpanel);
			
		} else {
        	if (month != calendar.get(Calendar.MONTH)) {
        		
        		line = 0;
        		year = calendar.get(Calendar.YEAR);
    	        month = calendar.get(Calendar.MONTH);
    	        day = calendar.get(Calendar.DATE);
    	        
    	        daysPane = new JTabbedPane(JTabbedPane.BOTTOM);
    	        
    	        monthsPane.addTab(getMonthNominative(month), daysPane);
    	        
    	        monthsPane.setSelectedComponent(daysPane);
    			
    			panel = new JPanel(); 
    	    	panel.setBorder(BorderFactory.createTitledBorder(Integer.toString(day) + " " + getMonthGenitive(month) + " " + Integer.toString(year)));
    	    	daysPane.add(Integer.toString(day), panel);
    	    	
    	    	daysPane.setSelectedComponent(panel);
    	    	
    			panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
    	        
    	    	USDpanel = new JPanel(new GridBagLayout());
    	    	USDpanel.setBorder(BorderFactory.createTitledBorder("������"));
    	    	panel.add(USDpanel);
    	    	
    	    	fill(USDpanel, new Color(255, 200, 200));
    	    	
    	    	EURpanel = new JPanel(new GridBagLayout());
    	    	EURpanel.setBorder(BorderFactory.createTitledBorder("����"));
    	    	panel.add(EURpanel);
    	    	
    	    	fill(EURpanel, new Color(200, 255, 255));
    	    	
    	    	RURpanel = new JPanel(new GridBagLayout());
    	    	RURpanel.setBorder(BorderFactory.createTitledBorder("�����"));
    	    	panel.add(RURpanel);
    	    	
    	    	fill(RURpanel, new Color(200, 255, 200));
    	    	
    	    	UAHpanel = new JPanel(new GridBagLayout());
    	    	UAHpanel.setBorder(BorderFactory.createTitledBorder("������"));
    	    	panel.add(UAHpanel);
    	        UAHpanel.setLayout(new GridBagLayout());
    	        
    	        fillDomestic(UAHpanel);
    	        
        	} else {
        		if (day != calendar.get(Calendar.DATE)) {
        			
        			line = 0;
        	        year = calendar.get(Calendar.YEAR);
        	        month = calendar.get(Calendar.MONTH);
        	        day = calendar.get(Calendar.DATE);
        			
        			panel = new JPanel(); 
        	    	panel.setBorder(BorderFactory.createTitledBorder(Integer.toString(day) + " " + getMonthGenitive(month) + " " + Integer.toString(year)));
        	    	daysPane.add(Integer.toString(day), panel);
        	    	
        			panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
        	        
        	    	USDpanel = new JPanel(new GridBagLayout());
        	    	USDpanel.setBorder(BorderFactory.createTitledBorder("������"));
        	    	panel.add(USDpanel);
        	    	
        	    	fill(USDpanel, new Color(255, 200, 200));
        	    	
        	    	EURpanel = new JPanel(new GridBagLayout());
        	    	EURpanel.setBorder(BorderFactory.createTitledBorder("����"));
        	    	panel.add(EURpanel);
        	    	
        	    	fill(EURpanel, new Color(200, 255, 255));
        	    	
        	    	RURpanel = new JPanel(new GridBagLayout());
        	    	RURpanel.setBorder(BorderFactory.createTitledBorder("�����"));
        	    	panel.add(RURpanel);
        	    	
        	    	fill(RURpanel, new Color(200, 255, 200));
        	    	
        	    	UAHpanel = new JPanel(new GridBagLayout());
        	    	UAHpanel.setBorder(BorderFactory.createTitledBorder("������"));
        	    	panel.add(UAHpanel);
        	        UAHpanel.setLayout(new GridBagLayout());
        	        
        	        fillDomestic(UAHpanel);
        	        
        		}
        	}
        }
        
    	JButton button;
        
        button = new JButton("�������� �������");
    	gbc.gridwidth = 4;
    	gbc.gridx = 0;
    	gbc.gridy = 3;
    	USDpanel.add(button, gbc);
        button.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent e)
            {
    			JPanel panel = new JPanel(new GridBagLayout());
    			JTextField textfield;
    			JLabel label;
    			Font font;
    	        GridBagConstraints gbc = new GridBagConstraints();
    	        gbc.fill = GridBagConstraints.HORIZONTAL;
    			
    	    	label = new JLabel("������");
    	    	font = label.getFont();
    	    	label.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 0;
    	    	gbc.gridy = 0;
    	    	panel.add(label, gbc);
    	    	
    	    	label = new JLabel("����");
    	    	font = label.getFont();
    	    	label.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 1;
    	    	gbc.gridy = 0;
    	    	panel.add(label, gbc);
    	    	
    	    	label = new JLabel("����������");
    	    	font = label.getFont();
    	    	label.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 2;
    	    	gbc.gridy = 0;
    	    	panel.add(label, gbc);
    	    	
    	    	textfield = new JTextField("");
    	    	font = textfield.getFont();
    	    	textfield.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 0;
    	    	gbc.gridy = 1;
    	    	panel.add(textfield, gbc);
    	    	
    	    	textfield = new JTextField("");
    	    	font = textfield.getFont();
    	    	textfield.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 1;
    	    	gbc.gridy = 1;
    	    	panel.add(textfield, gbc);
    	    	
    	    	textfield = new JTextField("");
    	    	font = textfield.getFont();
    	    	textfield.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 2;
    	    	gbc.gridy = 1;
    	    	panel.add(textfield, gbc);
    	    	
    	    	JButton button = new JButton("��������");
    	    	font = button.getFont();
    	    	button.setFont(font.deriveFont((float)24));
    	    	gbc.gridwidth = 3;
    	    	gbc.gridx = 0;
    	    	gbc.gridy = 2;
    	    	panel.add(button, gbc);
    	    	
    			JDialog dialog = new JDialog(frame, true);
    			dialog.add(panel);
    			dialog.pack();
                dialog.setVisible(true);
            }
        });
    	
    	button = new JButton("�������� �������");
    	gbc.gridwidth = 4;
    	gbc.gridx = 4;
    	gbc.gridy = 3;
    	USDpanel.add(button, gbc);
    	button.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent e)
            {
    			JPanel panel = new JPanel(new GridBagLayout());
    			JTextField textfield;
    			JLabel label;
    			Font font;
    	        GridBagConstraints gbc = new GridBagConstraints();
    	        gbc.fill = GridBagConstraints.HORIZONTAL;
    			
    	    	label = new JLabel("������");
    	    	font = label.getFont();
    	    	label.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 0;
    	    	gbc.gridy = 0;
    	    	panel.add(label, gbc);
    	    	
    	    	label = new JLabel("����");
    	    	font = label.getFont();
    	    	label.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 1;
    	    	gbc.gridy = 0;
    	    	panel.add(label, gbc);
    	    	
    	    	label = new JLabel("����������");
    	    	font = label.getFont();
    	    	label.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 2;
    	    	gbc.gridy = 0;
    	    	panel.add(label, gbc);
    	    	
    	    	textfield = new JTextField("");
    	    	font = textfield.getFont();
    	    	textfield.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 0;
    	    	gbc.gridy = 1;
    	    	panel.add(textfield, gbc);
    	    	
    	    	textfield = new JTextField("");
    	    	font = textfield.getFont();
    	    	textfield.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 1;
    	    	gbc.gridy = 1;
    	    	panel.add(textfield, gbc);
    	    	
    	    	textfield = new JTextField("");
    	    	font = textfield.getFont();
    	    	textfield.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 2;
    	    	gbc.gridy = 1;
    	    	panel.add(textfield, gbc);
    	    	
    	    	JButton button = new JButton("��������");
    	    	font = button.getFont();
    	    	button.setFont(font.deriveFont((float)24));
    	    	gbc.gridwidth = 3;
    	    	gbc.gridx = 0;
    	    	gbc.gridy = 2;
    	    	panel.add(button, gbc);
    	    	
    			JDialog dialog = new JDialog(frame, true);
    			dialog.add(panel);
    			dialog.pack();
                dialog.setVisible(true);
            }
        });
    	
    	button = new JButton("�������� �������");
    	gbc.gridwidth = 4;
    	gbc.gridx = 0;
    	gbc.gridy = 3;
    	EURpanel.add(button, gbc);
    	button.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent e)
            {
    			JPanel panel = new JPanel(new GridBagLayout());
    			JTextField textfield;
    			JLabel label;
    			Font font;
    	        GridBagConstraints gbc = new GridBagConstraints();
    	        gbc.fill = GridBagConstraints.HORIZONTAL;
    			
    	    	label = new JLabel("������");
    	    	font = label.getFont();
    	    	label.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 0;
    	    	gbc.gridy = 0;
    	    	panel.add(label, gbc);
    	    	
    	    	label = new JLabel("����");
    	    	font = label.getFont();
    	    	label.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 1;
    	    	gbc.gridy = 0;
    	    	panel.add(label, gbc);
    	    	
    	    	label = new JLabel("����������");
    	    	font = label.getFont();
    	    	label.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 2;
    	    	gbc.gridy = 0;
    	    	panel.add(label, gbc);
    	    	
    	    	textfield = new JTextField("");
    	    	font = textfield.getFont();
    	    	textfield.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 0;
    	    	gbc.gridy = 1;
    	    	panel.add(textfield, gbc);
    	    	
    	    	textfield = new JTextField("");
    	    	font = textfield.getFont();
    	    	textfield.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 1;
    	    	gbc.gridy = 1;
    	    	panel.add(textfield, gbc);
    	    	
    	    	textfield = new JTextField("");
    	    	font = textfield.getFont();
    	    	textfield.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 2;
    	    	gbc.gridy = 1;
    	    	panel.add(textfield, gbc);
    	    	
    	    	JButton button = new JButton("��������");
    	    	font = button.getFont();
    	    	button.setFont(font.deriveFont((float)24));
    	    	gbc.gridwidth = 3;
    	    	gbc.gridx = 0;
    	    	gbc.gridy = 2;
    	    	panel.add(button, gbc);
    	    	
    			JDialog dialog = new JDialog(frame, true);
    			dialog.add(panel);
    			dialog.pack();
                dialog.setVisible(true);
            }
        });
    	
    	button = new JButton("�������� �������");
    	gbc.gridwidth = 4;
    	gbc.gridx = 4;
    	gbc.gridy = 3;
    	EURpanel.add(button, gbc);
    	button.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent e)
            {
    			JPanel panel = new JPanel(new GridBagLayout());
    			JTextField textfield;
    			JLabel label;
    			Font font;
    	        GridBagConstraints gbc = new GridBagConstraints();
    	        gbc.fill = GridBagConstraints.HORIZONTAL;
    			
    	    	label = new JLabel("������");
    	    	font = label.getFont();
    	    	label.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 0;
    	    	gbc.gridy = 0;
    	    	panel.add(label, gbc);
    	    	
    	    	label = new JLabel("����");
    	    	font = label.getFont();
    	    	label.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 1;
    	    	gbc.gridy = 0;
    	    	panel.add(label, gbc);
    	    	
    	    	label = new JLabel("����������");
    	    	font = label.getFont();
    	    	label.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 2;
    	    	gbc.gridy = 0;
    	    	panel.add(label, gbc);
    	    	
    	    	textfield = new JTextField("");
    	    	font = textfield.getFont();
    	    	textfield.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 0;
    	    	gbc.gridy = 1;
    	    	panel.add(textfield, gbc);
    	    	
    	    	textfield = new JTextField("");
    	    	font = textfield.getFont();
    	    	textfield.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 1;
    	    	gbc.gridy = 1;
    	    	panel.add(textfield, gbc);
    	    	
    	    	textfield = new JTextField("");
    	    	font = textfield.getFont();
    	    	textfield.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 2;
    	    	gbc.gridy = 1;
    	    	panel.add(textfield, gbc);
    	    	
    	    	JButton button = new JButton("��������");
    	    	font = button.getFont();
    	    	button.setFont(font.deriveFont((float)24));
    	    	gbc.gridwidth = 3;
    	    	gbc.gridx = 0;
    	    	gbc.gridy = 2;
    	    	panel.add(button, gbc);
    	    	
    			JDialog dialog = new JDialog(frame, true);
    			dialog.add(panel);
    			dialog.pack();
                dialog.setVisible(true);
            }
        });
    	
    	button = new JButton("�������� �������");
    	gbc.gridwidth = 4;
    	gbc.gridx = 0;
    	gbc.gridy = 3;
    	RURpanel.add(button, gbc);
    	button.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent e)
            {
    			JPanel panel = new JPanel(new GridBagLayout());
    			JTextField textfield;
    			JLabel label;
    			Font font;
    	        GridBagConstraints gbc = new GridBagConstraints();
    	        gbc.fill = GridBagConstraints.HORIZONTAL;
    			
    	    	label = new JLabel("������");
    	    	font = label.getFont();
    	    	label.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 0;
    	    	gbc.gridy = 0;
    	    	panel.add(label, gbc);
    	    	
    	    	label = new JLabel("����");
    	    	font = label.getFont();
    	    	label.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 1;
    	    	gbc.gridy = 0;
    	    	panel.add(label, gbc);
    	    	
    	    	label = new JLabel("����������");
    	    	font = label.getFont();
    	    	label.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 2;
    	    	gbc.gridy = 0;
    	    	panel.add(label, gbc);
    	    	
    	    	textfield = new JTextField("");
    	    	font = textfield.getFont();
    	    	textfield.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 0;
    	    	gbc.gridy = 1;
    	    	panel.add(textfield, gbc);
    	    	
    	    	textfield = new JTextField("");
    	    	font = textfield.getFont();
    	    	textfield.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 1;
    	    	gbc.gridy = 1;
    	    	panel.add(textfield, gbc);
    	    	
    	    	textfield = new JTextField("");
    	    	font = textfield.getFont();
    	    	textfield.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 2;
    	    	gbc.gridy = 1;
    	    	panel.add(textfield, gbc);
    	    	
    	    	JButton button = new JButton("��������");
    	    	font = button.getFont();
    	    	button.setFont(font.deriveFont((float)24));
    	    	gbc.gridwidth = 3;
    	    	gbc.gridx = 0;
    	    	gbc.gridy = 2;
    	    	panel.add(button, gbc);
    	    	
    			JDialog dialog = new JDialog(frame, true);
    			dialog.add(panel);
    			dialog.pack();
                dialog.setVisible(true);
            }
        });
    	
    	button = new JButton("�������� �������");
    	gbc.gridwidth = 4;
    	gbc.gridx = 4;
    	gbc.gridy = 3;
    	RURpanel.add(button, gbc);
    	button.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent e)
            {
    			JPanel panel = new JPanel(new GridBagLayout());
    			JTextField textfield;
    			JLabel label;
    			Font font;
    	        GridBagConstraints gbc = new GridBagConstraints();
    	        gbc.fill = GridBagConstraints.HORIZONTAL;
    			
    	    	label = new JLabel("������");
    	    	font = label.getFont();
    	    	label.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 0;
    	    	gbc.gridy = 0;
    	    	panel.add(label, gbc);
    	    	
    	    	label = new JLabel("����");
    	    	font = label.getFont();
    	    	label.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 1;
    	    	gbc.gridy = 0;
    	    	panel.add(label, gbc);
    	    	
    	    	label = new JLabel("����������");
    	    	font = label.getFont();
    	    	label.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 2;
    	    	gbc.gridy = 0;
    	    	panel.add(label, gbc);
    	    	
    	    	textfield = new JTextField("");
    	    	font = textfield.getFont();
    	    	textfield.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 0;
    	    	gbc.gridy = 1;
    	    	panel.add(textfield, gbc);
    	    	
    	    	textfield = new JTextField("");
    	    	font = textfield.getFont();
    	    	textfield.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 1;
    	    	gbc.gridy = 1;
    	    	panel.add(textfield, gbc);
    	    	
    	    	textfield = new JTextField("");
    	    	font = textfield.getFont();
    	    	textfield.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 2;
    	    	gbc.gridy = 1;
    	    	panel.add(textfield, gbc);
    	    	
    	    	JButton button = new JButton("��������");
    	    	font = button.getFont();
    	    	button.setFont(font.deriveFont((float)24));
    	    	gbc.gridwidth = 3;
    	    	gbc.gridx = 0;
    	    	gbc.gridy = 2;
    	    	panel.add(button, gbc);
    	    	
    			JDialog dialog = new JDialog(frame, true);
    			dialog.add(panel);
    			dialog.pack();
                dialog.setVisible(true);
            }
        });
    	
    	button = new JButton("��������");
    	gbc.gridwidth = 8;
    	gbc.gridx = 0;
    	gbc.gridy = 2;
    	UAHpanel.add(button, gbc);
    	button.addActionListener(new ActionListener() {
    		public void actionPerformed(ActionEvent e) {
    			
    			JPanel panel = new JPanel(new GridBagLayout());
    			JTextField textfield;
    			JLabel label;
    			Font font;
    	        GridBagConstraints gbc = new GridBagConstraints();
    	        gbc.fill = GridBagConstraints.HORIZONTAL;
    			
    	    	label = new JLabel("������");
    	    	font = label.getFont();
    	    	label.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 0;
    	    	gbc.gridy = 0;
    	    	panel.add(label, gbc);
    	    	
    	    	label = new JLabel("����");
    	    	font = label.getFont();
    	    	label.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 1;
    	    	gbc.gridy = 0;
    	    	panel.add(label, gbc);
    	    	
    	    	label = new JLabel("����������");
    	    	font = label.getFont();
    	    	label.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 2;
    	    	gbc.gridy = 0;
    	    	panel.add(label, gbc);
    	    	
    	    	textfield = new JTextField("");
    	    	font = textfield.getFont();
    	    	textfield.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 0;
    	    	gbc.gridy = 1;
    	    	panel.add(textfield, gbc);
    	    	
    	    	textfield = new JTextField("");
    	    	font = textfield.getFont();
    	    	textfield.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 1;
    	    	gbc.gridy = 1;
    	    	panel.add(textfield, gbc);
    	    	
    	    	textfield = new JTextField("");
    	    	font = textfield.getFont();
    	    	textfield.setFont(font.deriveFont(Font.PLAIN, (float)24));
    	    	gbc.gridx = 2;
    	    	gbc.gridy = 1;
    	    	panel.add(textfield, gbc);
    	    	
    	    	JButton button = new JButton("��������");
    	    	font = button.getFont();
    	    	button.setFont(font.deriveFont((float)24));
    	    	gbc.gridwidth = 3;
    	    	gbc.gridx = 0;
    	    	gbc.gridy = 2;
    	    	panel.add(button, gbc);
    	    	
    	    	button.addActionListener(new ActionListener() {
    	    		public void actionPerformed(ActionEvent e) {
    	    			
    	            }
    	        });
    	    	
    			JDialog dialog = new JDialog(frame, true);
    			dialog.add(panel);
    			dialog.pack();
                dialog.setVisible(true);
            }
        });
    	
        frame.pack();
        frame.setVisible(true);
        	
	}	
	
}        